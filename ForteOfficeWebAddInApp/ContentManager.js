﻿// <reference path="/Scripts/FabricUI/MessageBanner.js" />


(function () {
    "use strict";

    var messageBanner;
    // The initialize function must be run each time a new page is loaded.
    Office.initialize = function (reason) {
        $(document).ready(function () {
            //// Initialize the FabricUI notification mechanism and hide it
            //var element = document.querySelector('.ms-MessageBanner');
            //messageBanner = new fabric.MessageBanner(element);
            //messageBanner.hideBanner();
            $('#show-dlg').click(ShowDataInputForm);
            //$('#snav-win').click(ShowDataInputForm);
            $('#create-doc').click(CreateDocument);
            //initialize tree
            Word.run(function (context) {
                $('#tree').jstree({
                    'core': {
                        'data': GetTreeFolders
                    }
                });
            });
        });
    };

    function ShowDataInputForm() {
        var id = $('#tree').jstree(true).get_selected(true)[0].id;
        var segID = id.substring(id.indexOf(".") + 1, id.length);
        var dataToPassToService = {
            Type: "GetUIHtml",
            Args: 'Daniel Fisherman' + '~' + segID + '~'
        };

        $.ajax({
            url: 'api/ForteServer',
            type: 'POST',
            data: JSON.stringify(dataToPassToService),
            contentType: 'application/json;charset=utf-8'
        }).done(function (data) {
            document.write(data);
        }).fail(function (status) {
            showNotification("Request failed.", status);
        }).always(function () {
            // placeholdere
        });
    }
    // Reads data from current document selection and displays a notification
    function CreateDocument() {
        Word.run(function (context) {
            var id = $('#tree').jstree(true).get_selected(true)[0].id;
            var segID = id.substring(id.indexOf(".") + 1, id.length);
            var dataToPassToService = {
                Type: "CreateDocument",
                Args: segID + "~" + $('#prefill').val()
            };

            $.ajax({
                url: 'api/ForteServer',
                type: 'POST',
                data: JSON.stringify(dataToPassToService),
                contentType: 'application/json;charset=utf-8'
            }).done(function (data) {
                //context.document.body.insertOoxml(data, "end")
                //does not insert headers/footers by design - see http://tickanswer.com/solved/2389978642/header-not-showing-up-when-im-inserting-a-document
                context.document.body.insertFileFromBase64(data, "End");
                context.sync();
                showNotification("Request complete.", "");
            }).fail(function (status) {
                showNotification("Request failed.", status);
            }).always(function () {
                // placeholder
            });
        });
    }

    // Reads data from current document selection and displays a notification
    function GetTreeFolders(node, callback) {
        $.ajax({
            url: 'api/ForteServer',
            type: 'POST',
            data: { Type: "GetFolderMembersJSON", Args: "Daniel Fisherman~" + node.id },
            dataType: 'json'
        }).success(function (ret) {
            var json = eval("(" + ret + ")");
            callback.call(this, json);
        }).fail(function (status) {
                showNotification("Request failed.", status);
        }).always(function () {
            // placeholder
        });
    }

    //$$(Helper function for treating errors, $loc_script_taskpane_home_js_comment34$)$$
    function errorHandler(error) {
        // $$(Always be sure to catch any accumulated errors that bubble up from the Word.run execution., $loc_script_taskpane_home_js_comment35$)$$
        showNotification("Error:", error);
        console.log("Error: " + error);
        if (error instanceof OfficeExtension.Error) {
            console.log("Debug info: " + JSON.stringify(error.debugInfo));
        }
    }

    // Helper function for displaying notifications
    function showNotification(header, content) {
        $("#status-header").text(header);
        $("#status-body").text(content);
        //messageBanner.showBanner();
        //messageBanner.toggleExpansion();
    }
})();
