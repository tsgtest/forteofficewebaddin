<!DOCTYPE html >
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="content-type" content="text/html; charset=ISO-8859-1" />
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery.fancytree/2.22.1/skin-win8/ui.fancytree.min.css" />
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.1/jquery.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.fancytree/2.22.1/jquery.fancytree.js"></script>
      <script>
$(function () {
$('#segmentTree').fancytree( {
                })
                });         
</script>
      <style type="text/css">
textarea {
width: 40em;
}
input {
width: 39em;
}
select {
width: 25em;
}
select.checkbox {
width: 15em;
}
li.variable-item > a > .tree-icon
{
font-size: 0.7em;
color: #999999;
}
</style>
   </head>
   <body>
      <table width="50%" cellpadding="5" border="1">
         <tr>
            <td>
               <div id="segmentTree">
                  <ul class="segment-list">
                     <li class="segment-item-top expanded jstree-open" id="Letter_{A9BC08B606984242B5BD0CD445799F83}" data-jstree="{&quot;icon&quot;:&quot;fa fa-file-word-o&quot;}">
                        <a>Letter</a>
                        <ul class="variable-list">
                           <li class="variable-item expanded jstree-open" id="271fb163-8e1a-454f-b035-f158c7b03d29" data-jstree="{&quot;icon&quot;:&quot;fa fa-circle&quot;}">
                              <a>Date</a>
                              <ul class="control-list">
                                 <li class="variable-control" data-icon="">
                                    <select id="input_271fb163-8e1a-454f-b035-f158c7b03d29">
                                       <option value="MMMM d, yyyy" selected="true">MMMM d, yyyy</option>
                                       <option value="MMMM d, yyyy /F">MMMM d, yyyy /F</option>
                                       <option value="MM/dd/yy">MM/dd/yy</option>
                                       <option value="MM/dd/yy /F">MM/dd/yy /F</option>
                                       <option value="MMMM ___, yyyy">MMMM ___, yyyy</option>
                                    </select>
                                 </li>
                              </ul>
                           </li>
                           <li class="variable-item expanded jstree-open" id="916eea1c-5241-4995-84c0-56f5a1a5c4f8" data-jstree="{&quot;icon&quot;:&quot;fa fa-circle&quot;}">
                              <a>Delivery Phrases</a>
                              <ul class="control-list">
                                 <li class="variable-control" data-icon="">
                                    <textarea id="input_916eea1c-5241-4995-84c0-56f5a1a5c4f8" rows="3" cols="60" style="resize:none"></textarea>
                                    <p class="list-para">
                                       <select id="list_916eea1c-5241-4995-84c0-56f5a1a5c4f8">
                                          <option value=""></option>
                                          <option value="Attorney Work Product">Attorney Work Product</option>
                                          <option value="Attorney-Client Privilege">Attorney-Client Privilege</option>
                                          <option value="By Certified Mail&#xD;&#xA;Return Receipt Requested">By Certified Mail
Return Receipt Requested</option>
                                          <option value="By Registered Mail&#xD;&#xA;Return Receipt Requested">By Registered Mail
Return Receipt Requested</option>
                                          <option value="Confidential">Confidential</option>
                                          <option value="Draft">Draft</option>
                                          <option value="Via E-Mail">E-Mail</option>
                                          <option value="Via Express Mail">Express Mail</option>
                                          <option value="Via Facsimile">Facsimile</option>
                                          <option value="Via Facsimile and Mail">Facsimile and Mail</option>
                                          <option value="Via Federal Express">Federal Express</option>
                                          <option value="Via FedEx">FedEx</option>
                                          <option value="By Hand">Hand</option>
                                          <option value="By Hand Delivery">Hand Delivery</option>
                                          <option value="Joint Defense Privilege">Joint Defense Privilege</option>
                                          <option value="Via Overnight Courier">Overnight Courier</option>
                                          <option value="Personal">Personal</option>
                                          <option value="Personal &amp; Confidential">Personal &amp; Confidential</option>
                                          <option value="Privileged &amp; Confidential">Privileged &amp; Confidential</option>
                                          <option value="Privileged Document">Privileged Document</option>
                                          <option value="Return Receipt Requested">Return Receipt Requested</option>
                                          <option value="In Strict Confidence">Strict Confidence</option>
                                          <option value="Via UPS">UPS</option>
                                       </select>
                                    </p>
                                    <script type="text/javascript">
var mytextbox = document.getElementById('input_916eea1c-5241-4995-84c0-56f5a1a5c4f8');
var mydropdown = document.getElementById('list_916eea1c-5241-4995-84c0-56f5a1a5c4f8');
mydropdown.onchange = function(){
mytextbox.value = (mytextbox.value == '' ? this.value : mytextbox.value  + '\r' + this.value);
this.value = '';
}</script>
                                 </li>
                              </ul>
                           </li>
                           <li class="variable-item expanded jstree-open" id="d1d65843-db4f-4a33-aced-a71d5f21708e" data-jstree="{&quot;icon&quot;:&quot;fa fa-circle&quot;}">
                              <a>Recipients</a>
                              <ul class="control-list">
                                 <li class="variable-control" data-icon="">
                                    <textarea id="input_d1d65843-db4f-4a33-aced-a71d5f21708e" rows="3" cols="60" style="resize:none"></textarea>
                                 </li>
                              </ul>
                           </li>
                           <li class="variable-item expanded jstree-open" id="12027603-dcce-474d-a297-cbd82dcecd55" data-jstree="{&quot;icon&quot;:&quot;fa fa-circle&quot;}">
                              <a>Format for Recipients</a>
                              <ul class="control-list">
                                 <li class="variable-control" data-icon="">
                                    <select id="input_12027603-dcce-474d-a297-cbd82dcecd55" class="checkbox">
                                       <option value="true">Side-by-Side</option>
                                       <option value="false" selected="true">Stacked</option>
                                    </select>
                                 </li>
                              </ul>
                           </li>
                           <li class="variable-item expanded jstree-open" id="98530733-11a1-4dd9-8bc9-59eec5373853" data-jstree="{&quot;icon&quot;:&quot;fa fa-circle&quot;}">
                              <a>Re Line</a>
                              <ul class="control-list">
                                 <li class="variable-control" data-icon="">
                                    <textarea id="input_98530733-11a1-4dd9-8bc9-59eec5373853" rows="3" cols="60" style="resize:none"></textarea>
                                 </li>
                              </ul>
                           </li>
                           <li class="variable-item expanded jstree-open" id="8eee1355-cefc-46ae-bc86-ba2883eda5a3" data-jstree="{&quot;icon&quot;:&quot;fa fa-circle&quot;}">
                              <a>Client Matter No.</a>
                              <ul class="control-list">
                                 <li class="variable-control" data-icon="">
                                    <input id="input_8eee1355-cefc-46ae-bc86-ba2883eda5a3" type="text" value="" />
                                 </li>
                              </ul>
                           </li>
                           <li class="variable-item expanded jstree-open" id="407e3fda-8dd2-4911-afbf-85b5b80c1997" data-jstree="{&quot;icon&quot;:&quot;fa fa-circle&quot;}">
                              <a>Salutation</a>
                              <ul class="control-list">
                                 <li class="variable-control" data-icon="">
                                    <input id="input_407e3fda-8dd2-4911-afbf-85b5b80c1997" type="text" value="Dear :" />
                                 </li>
                              </ul>
                           </li>
                           <li class="variable-item expanded jstree-open" id="7076b25e-2a29-477d-a5e9-8c02f2887a1b" data-jstree="{&quot;icon&quot;:&quot;fa fa-circle&quot;}">
                              <a>Enclosures</a>
                              <ul class="control-list">
                                 <li class="variable-control" data-icon="">
                                    <select id="input_7076b25e-2a29-477d-a5e9-8c02f2887a1b">
                                       <option value=""></option>
                                       <option value="Attachment">Attachment</option>
                                       <option value="Attachments">Attachments</option>
                                       <option value="Enclosure">Enclosure</option>
                                       <option value="Enclosures">Enclosures</option>
                                    </select>
                                 </li>
                              </ul>
                           </li>
                           <li class="variable-item expanded jstree-open" id="7c3d737d-1464-4e66-aba2-abded3496334" data-jstree="{&quot;icon&quot;:&quot;fa fa-circle&quot;}">
                              <a>cc</a>
                              <ul class="control-list">
                                 <li class="variable-control" data-icon="">
                                    <textarea id="input_7c3d737d-1464-4e66-aba2-abded3496334" rows="3" cols="60" style="resize:none"></textarea>
                                 </li>
                              </ul>
                           </li>
                           <li class="variable-item expanded jstree-open" id="1ada7a96-ee64-47c3-a71b-109a32c6e690" data-jstree="{&quot;icon&quot;:&quot;fa fa-circle&quot;}">
                              <a>bcc</a>
                              <ul class="control-list">
                                 <li class="variable-control" data-icon="">
                                    <textarea id="input_1ada7a96-ee64-47c3-a71b-109a32c6e690" rows="3" cols="60" style="resize:none"></textarea>
                                 </li>
                              </ul>
                           </li>
                           <li class="variable-item expanded jstree-open" id="539f5b83-c497-45a1-a0bb-3ec2341611ce" data-jstree="{&quot;icon&quot;:&quot;fa fa-circle&quot;}">
                              <a>Author Initials</a>
                              <ul class="control-list">
                                 <li class="variable-control" data-icon="">
                                    <input id="input_539f5b83-c497-45a1-a0bb-3ec2341611ce" type="text" value="DCF" />
                                 </li>
                              </ul>
                           </li>
                           <li class="variable-item expanded jstree-open" id="3604bf8a-1317-45a1-b5fc-82f480920f6c" data-jstree="{&quot;icon&quot;:&quot;fa fa-circle&quot;}">
                              <a>Typist Initials</a>
                              <ul class="control-list">
                                 <li class="variable-control" data-icon="">
                                    <input id="input_3604bf8a-1317-45a1-b5fc-82f480920f6c" type="text" value="ljs" />
                                 </li>
                              </ul>
                           </li>
                           <li class="variable-item expanded jstree-open" id="c329bba8-c78d-49a9-9855-b94058e57d79" data-jstree="{&quot;icon&quot;:&quot;fa fa-circle&quot;}">
                              <a>Page 2 Header Text</a>
                              <ul class="control-list">
                                 <li class="variable-control" data-icon="">
                                    <textarea id="input_c329bba8-c78d-49a9-9855-b94058e57d79" rows="3" cols="60" style="resize:none"></textarea>
                                 </li>
                              </ul>
                           </li>
                           <li class="variable-item expanded jstree-open" id="c329bbc9-c78d-49a9-9855-b94058e56a79" data-jstree="{&quot;icon&quot;:&quot;fa fa-circle&quot;}">
                              <a>Include Delivery Phrases in Header</a>
                              <ul class="control-list">
                                 <li class="variable-control" data-icon="">
                                    <select id="input_c329bbc9-c78d-49a9-9855-b94058e56a79" class="checkbox">
                                       <option value="true" selected="true">Yes</option>
                                       <option value="false">No</option>
                                    </select>
                                 </li>
                              </ul>
                           </li>
                           <li class="segment-item" id="LetterSignaturesCC_{1A87648DACFF4783BF2C7049563E3B04}" data-jstree="{&quot;icon&quot;:&quot;fa fa-file-text-o&quot;}">
                              <a>Letter Signatures CC</a>
                              <ul class="variable-list">
                                 <li class="segment-item" id="StandardSignature_{FA1B5FE625C24092A3A750218DA54744}" data-jstree="{&quot;icon&quot;:&quot;fa fa-file-text-o&quot;}">
                                    <a>Standard Signature</a>
                                    <ul class="variable-list">
                                       <li class="variable-item expanded jstree-open" id="15c54f81-7303-46f1-aba9-b672c4b7b351" data-jstree="{&quot;icon&quot;:&quot;fa fa-circle&quot;}">
                                          <a>Closing Phrase</a>
                                          <ul class="control-list">
                                             <li class="variable-control" data-icon="">
                                                <select id="input_15c54f81-7303-46f1-aba9-b672c4b7b351">
                                                   <option value=""></option>
                                                   <option value="Best regards,">Best regards,</option>
                                                   <option value="Best wishes,">Best wishes,</option>
                                                   <option value="Cordially,">Cordially,</option>
                                                   <option value="Regards,">Regards,</option>
                                                   <option value="Respectfully submitted,">Respectfully submitted,</option>
                                                   <option value="Respectfully yours,">Respectfully yours,</option>
                                                   <option value="Respectfully,">Respectfully,</option>
                                                   <option value="Sincerely yours,">Sincerely yours,</option>
                                                   <option value="Sincerely,">Sincerely,</option>
                                                   <option value="Truly yours,">Truly yours,</option>
                                                   <option value="Very truly yours," selected="true">Very truly yours,</option>
                                                   <option value="Yours truly,">Yours truly,</option>
                                                   <option value="Yours very truly,">Yours very truly,</option>
                                                </select>
                                             </li>
                                          </ul>
                                       </li>
                                       <li class="variable-item expanded jstree-open" id="a78ed49f-212e-4e34-80cb-45c52ab8a62f" data-jstree="{&quot;icon&quot;:&quot;fa fa-circle&quot;}">
                                          <a>Include Firm Name</a>
                                          <ul class="control-list">
                                             <li class="variable-control" data-icon="">
                                                <select id="input_a78ed49f-212e-4e34-80cb-45c52ab8a62f" class="checkbox">
                                                   <option value="true">Yes</option>
                                                   <option value="false" selected="true">No</option>
                                                </select>
                                             </li>
                                          </ul>
                                       </li>
                                       <li class="variable-item expanded jstree-open" id="a3abb023-dbb3-4fd7-8957-386c481bb985" data-jstree="{&quot;icon&quot;:&quot;fa fa-circle&quot;}">
                                          <a>Include Title</a>
                                          <ul class="control-list">
                                             <li class="variable-control" data-icon="">
                                                <select id="input_a3abb023-dbb3-4fd7-8957-386c481bb985" class="checkbox">
                                                   <option value="true">Yes</option>
                                                   <option value="false" selected="true">No</option>
                                                </select>
                                             </li>
                                          </ul>
                                       </li>
                                    </ul>
                                 </li>
                              </ul>
                           </li>
                        </ul>
                     </li>
                  </ul>
               </div>
            </td>
         </tr>
      </table>
   </body>
</html>